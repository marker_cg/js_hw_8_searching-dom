//1

const paragraphs = document.getElementsByTagName('p');

for (let elements of paragraphs) {
    elements.style.backgroundColor = '#ff0000';
}

//2

const optionList = document.getElementById('optionsList');
const parentOptionList = optionList.parentElement;
const childNodesList = optionList.childNodes;


console.log(optionList);
console.log(parentOptionList);

console.log(optionList.hasChildNodes());
for (let child of childNodesList) {
    console.log(child)
}

//3
const testParagraph = document.getElementById('testParagraph');

testParagraph.textContent = 'This is a paragraph';
console.log(testParagraph)

//4

const mainHeader = document.querySelector('.main-header')
console.log(mainHeader)

const mainHeaderChilds = mainHeader.children;
console.log(mainHeaderChilds);


for (let child of mainHeaderChilds) {
    child.classList.add('nav-item')
}
console.log(mainHeaderChilds)

//5

const sectionTitle = document.querySelectorAll('.section-title')
console.log(sectionTitle)


for (let elements of sectionTitle) {
    elements.classList.remove('section-title')
}


console.log(sectionTitle)










